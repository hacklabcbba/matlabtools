function [str] = motion_export_to_c(p)
[m,n] = size(p);
str = '';
for i=1:m
    for j=1:n
        str = sprintf('%ssystem[%d][%d] = %s;\n', str, i-1, j-1, char(p(i,j)));
    end
end
str = strrep(str, '^', '_');
str = strrep(str, 'dt1', 'dt0');
str = strrep(str, 'dt2', 'dt1');
str = strrep(str, 'dt3', 'dt2');
str = strrep(str, 'dt4', 'dt3');
str = strrep(str, 'dt5', 'dt4');
str = strrep(str, 'dt6', 'dt5');
str = strrep(str, 'dt7', 'dt6');
str = strrep(str, 'dt8', 'dt7');
str = strrep(str, 'dt9', 'dt8');

str = strrep(str, 'r1', 'c0');
str = strrep(str, 'r2', 'c1');
str = strrep(str, 'v1', 'c2');
str = strrep(str, 'v2', 'c3');
str = strrep(str, 'a1', 'c4');
str = strrep(str, 'a2', 'c5');
str = strrep(str, 'j1', 'c6');
str = strrep(str, 'j2', 'c7');
str = strrep(str, 's1', 'c8');
str = strrep(str, 's2', 'c9');
end