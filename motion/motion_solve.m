function [p, A, b, x] = motion_solve(cfg)
nvar = sum(cfg{1}+1);
nseg = numel(cfg{1});
ncof = cfg{1} + 1;
mcof = max(ncof);

%% Create Matrix
A = [];
for i=1:nseg
    n = cfg{2}(:,i);
    t = cfg{3}(:,i);
    [c, e] = motion_matrix_gen(cfg{1}(i));
    nmax = max(abs(n));
    if size(c,1) < nmax
        c(nmax,:) = 0;
        e(nmax,:) = 0;
    end
    A = [A, motion_segment_gen(c,e,n,t)];
end
b = cfg{4};

%% Solve
x = A\b;

%% Process
if isa(x, 'sym')
    xi = sym(zeros(nseg,mcof));
else
    xi = zeros(nseg,mcof);
end
n = 1;
for i=1:nseg
    xi(i,end-cfg{1}(i):end) = x(n:n+cfg{1}(i));
    n = n + ncof(i);
end

p = xi.*repmat(c(1,:),nseg,1);
end

