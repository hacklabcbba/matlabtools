clear

%% Config
r = [0 500];
v = [0 0];
a = [0 0];
j = [0 0];
tt = 2.5;
dt1 = tt/(2*1); dt2 = tt/(2*2);

dg = [6 1 6];
dt = [dt2 dt1 dt2];
t0 = 0;

%% Init/Config
ts = cumsum([t0 dt]);
ts = ts(:);
nvar = sum(dg+1);
nseg = numel(dg);
ncof = dg+1;
mcof = max(ncof);

cfg{1} = zeros(1,nseg);
cfg{2} = zeros(nvar,nseg);
cfg{3} = zeros(nvar,nseg);
cfg{4} = zeros(nvar,1);

cfg{1} = dg;
cfg{2} = [
    1  0  0;
    2  0  0;
    3  0  0;
    4  0  0;
    0  0  1;
    0  0  2;
    0  0  3;
    0  0  4;
    
    1 -1  0;
    2 -2  0;
    3 -3  0;
    4 -4  0;
    0  1 -1;
    0  2 -2;
    0  3 -3;
    0  4 -4;
    ];

cfg{3} = [
    0      0      0;
    0      0      0;
    0      0      0;
    0      0      0;
    0      0      dt(3);
    0      0      dt(3);
    0      0      dt(3);
    0      0      dt(3);
    
    dt(1)  0      0;
    dt(1)  0      0;
    dt(1)  0      0;
    dt(1)  0      0;
    0      dt(2)  0;
    0      dt(2)  0;
    0      dt(2)  0;
    0      dt(2)  0;
    ];

cfg{4}([1:8]) = [r(1) v(1) a(1) j(1) r(2) v(2) a(2) j(2)];

%% Create & solve system
[p, A, b, x] = motion_solve(cfg);

%% Derive system
dp = motion_deriv(p,mcof);
for i=1:nseg
    f{1} = @(t)polyval(p(i,:),t-ts(i));
    for j=1:numel(dp{1})
        f{j+1} = @(t)polyval(dp{i}{j},t-ts(i));
    end
    fi{i} = f;
end

%% Plot
tn = [ts(1:end-1) ts(2:end)];
ti = {};
for i=1:size(tn,1)
    ti{i} = [tn(i,1):1e-3:tn(i,2)];
end
motion_plot(fi,ti,4);
