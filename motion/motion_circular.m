r = 1;
d2s = 20

fx = @(w,x0) x0+r*cos(w);
fy = @(w,y0) y0+r*sin(w);

t = 0:1e-3:2*pi;

x = fx(t,1);
y = fy(t,-1);

figure
subplot(2,1,1)
grid on, hold all
stairs(x,y)
pbaspect([1 1 1])

subplot(2,1,2)
grid on, hold all
stairs(t,x)
stairs(t,y)


c1 = sin(d2s*(2*pi*x/4));
c2 = cos(d2s*(2*pi*x/4));
c3 = sin(d2s*(2*pi*y/4));
c4 = cos(d2s*(2*pi*y/4));

figure
subplot(2,1,1)
grid on, hold all
stairs(t,c1)
stairs(t,c2)

subplot(2,1,2)
grid on, hold all
stairs(t,c3)
stairs(t,c4)