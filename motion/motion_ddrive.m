figure
hold all, grid on

for i=1:numel(ts)-1
    t = ts(i):1e-4:ts(i+1);
    x = fi{i}{1}(t);
    
    c1 = sin(2*pi*x/4);
    c2 = cos(2*pi*x/4);
    
    plot(t,c1)
    plot(t,c2)
end




