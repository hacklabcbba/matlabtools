function [dp] = motion_deriv(p,n)
% [dp] = motion_deriv(p,n)

for i=1:size(p,1)
    dip{1} = polyder(p(i,:));
    for j=2:n
        dip{j} = polyder(dip{j-1});
    end
    dp{i} = dip;
end
end
