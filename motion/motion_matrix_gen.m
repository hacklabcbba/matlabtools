function [c,e] = motion_matrix_gen(d)
% [c,e] = motion_matrix_gen(d)

%% Matrix generator
sz = d + 1;
e = zeros(sz);
c = eye(sz);
for i=2:sz
    for j=1:i-1
        e(i,j) = i - j;
        c(i,j) = c(i-1,j)/e(i,j);
    end
end
e = flipud(e);
c = flipud(c);
end

