i = 1;
xa = ti{i}(1);
xb = ti{i}(end);
tol = 1e-14;
imax = 200;
c = 46;
f = fi{i}{1};

tsteps = (log(xb-xa)-log(tol))/(log(2))
[x, y, cflag, eflag, icnt] = bisection(f, c, xa, xb, tol, imax)
[x, y, cflag, eflag, icnt] = regulafalsi(f, c, xa, xb, tol, imax)
[x, y, cflag, eflag, icnt] = regulafalsi_mod(f, c, xa, xb, tol, imax)
% x = fzero(@(t)f(t)-c,[xa xb],optimset('Display','iter'));
