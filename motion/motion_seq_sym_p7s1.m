clear

%% Config
dg = [7];
dt = sym('dt',[1 1]);
r = sym('r',[1 2]);
v = sym('v',[1 2]);
a = sym('a',[1 2]);
j = sym('j',[1 2]);

%% Init/Config
nvar = sum(dg+1);
nseg = numel(dg);
ncof = dg+1;
mcof = max(ncof);

cfg{1} = zeros(1,nseg);
cfg{2} = zeros(nvar,nseg);
cfg{3} = zeros(nvar,nseg);
cfg{4} = sym(zeros(nvar,1));

cfg{1} = dg;
cfg{2} = [
    1;
    2;
    3;
    4;
    
    1;
    2;
    3;
    4;
    ];

cfg{3} = [
    0;
    0;
    0;
    0;
    
    dt(1);
    dt(1);
    dt(1);
    dt(1);
    ];

cfg{4}([1:8]) = [r(1) v(1) a(1) j(1) r(2) v(2) a(2) j(2)];

%% Create & solve system
[p, A, b, x] = motion_solve(cfg);

%% Export
str = motion_export_to_c(p);
fid = fopen('motion_p7s1.txt','wt');
fprintf(fid, str);
fclose(fid);
