function motion_plot(fi,ti,n)
% motion_plot(fi,ti,n)

%% Plot %%
nplot = min(n, numel(fi{1}));
fig_handler = figure;

for i=1:numel(fi)
    for j=1:nplot
        subplot(nplot,1,j)
        hold all; grid on;
        plot(ti{i},fi{i}{j}(ti{i}))
    end
end
xlim([ti{1}(1),ti{end}(end)])
all_ha = findobj(fig_handler, 'type', 'axes', 'tag', '');
linkaxes(all_ha, 'x' );
end
