function [A] = motion_segment_gen(c,e,n,t)
% [A] = motion_segment_gen(c,e,n,t)

%% Init
nequ = numel(n);
ncof = size(c,2);
if isa(t, 'sym')
    A = sym(zeros(nequ, ncof));
else
    A = zeros(nequ, ncof);
end

%% Calc
list = find(n~=0);
sl = sign(n(list));
nl = abs(n(list));
tl = t(list);

a = diag(sl)*c(nl,:);
for i=1:size(e,2)
    T(:,i) = tl.^e(nl,i);
end
A(list,:) = (a.*T);
end
