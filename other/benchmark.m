%% Single point (1) cortex-m3 DP
freq = 72e6;
m3_dp_opt0 = [121877 47092]*(1)/(freq);
m3_dp_opt3 = [91197 45955]*(1)/(freq);

%% Single point (1) cortex-m3 SP
freq = 72e6;
m3_sp_opt0 = [23673 17789]*(1)/(freq);
m3_sp_opt3 = [18279 16783]*(1)/(freq);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Single point (1) cortex-m4 DP
freq = 84e6;
m4_dp_opt0 = [90062 33927]*(1)/(freq);
m4_dp_opt3 = [61749 32481]*(1)/(freq);

%% Single point (1) cortex-m4 SP
freq = 84e6;
m4_sp_opt0 = [16612 12547]*(1)/(freq);
m4_sp_opt3 = [11979 11455]*(1)/(freq);

%% Single point (1) cortex-m4 SP FPU
freq = 84e6;
m4_sp_opt0_fpu = [3782 2526]*(1)/(freq);
m4_sp_opt3_fpu = [1490 1277]*(1)/(freq);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Single point (1) cortex-m7 DP
freq = 216e6;
m7_dp_opt0 = [75170 30468]*(1)/(freq);
m7_dp_opt3 = [45384 29124]*(1)/(freq);

%% Single point (1) cortex-m7 SP
freq = 216e6;
m7_sp_opt0 = [15344 11905]*(1)/(freq);
m7_sp_opt3 = [10816 10679]*(1)/(freq);

%% Single point (1) cortex-m7 SP FPU
freq = 216e6;
m7_sp_opt0_fpu = [3820 2659]*(1)/(freq);
m7_sp_opt3_fpu = [1787 1528]*(1)/(freq);
