function [t,x,s,data] = stepper(file, dplot)
% [t,s,data] = stepper(file)

data = log_extract(file);
t = data(:,1);
x = cumsum(ones(size(t)).*data(:,2));
s = deriv(t,x,dplot);

ts = [t(1):0.01:t(end)]';
xs = spline(t,x,ts);
ss = deriv(ts,xs,dplot);

graph(t,s,ts,ss);
end

function [data] = log_extract(file)
load(file);
sample_rate = digital_sample_rate_hz;
step_channel = digital_channel_0;
dir_channel = digital_channel_1;
initial_state = digital_channel_initial_bitstates;

step_raw = zeros(length(step_channel), 2);
step_raw(1:2:end,1) = initial_state(1);
step_raw(2:2:end,1) = ~initial_state(1);
step_raw(2:end,2)   = cumsum(step_channel(1:end-1));

dir_raw = zeros(length(dir_channel), 1);
dir_raw(2:end,1)   = cumsum(dir_channel(1:end-1));

% en_raw = zeros(length(en_channel), 1);
% en_raw(2:end,1)   = cumsum(end_channel(1:end-1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

idx = find(step_raw(:,1) == 1);
data      = zeros(length(idx), 3);
data(:,1) = step_raw(idx,2);

dir_range = bsxfun(@ge, data(:,1), dir_raw(:,1)') & bsxfun(@lt, data(:,1), [dir_raw(2:end,1); num_samples_digital]');
if(digital_channel_initial_bitstates(2))
    dir_tmp = sum(dir_range(:,1:2:end),2);
else
    dir_tmp = sum(dir_range(:,2:2:end),2);
end
dir_tmp(dir_tmp == 0) = -1;
data(:,1) = data(:,1)*inv(sample_rate);
data(:,2) = dir_tmp;
end

function [s] = deriv (t,x,n)
s = [x];
for i=1:n
    dx = gradient(s(:,i))./gradient(t);
    s = [s,dx];
end
end

function graph (t,s,ts,ss)
figure_handle = figure;
nplot = size(ss,2);
for i=1:nplot
    splot_handle(i) = subplot(nplot,1,i);
    hold all;
    grid on;
    plot(t, s(:,i), '.')
    plot(ts, ss(:,i))
end

all_ha = findobj( figure_handle, 'type', 'axes', 'tag', '' );
linkaxes( all_ha, 'x' );
end