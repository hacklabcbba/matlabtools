function [str] = export_array(A, name)
% [str] = export_array(A, name)

dims = size(A);
ndims = numel(dims);

if ~isstr(name)
    name = 'A';
end
str = sprintf('double %s', name);
for i=1:ndims
    str = sprintf('%s[%d]', str, dims(i));
end
cont = export_row(A, dims);
str = sprintf('%s = {\n%s\n};',str, cont);
end

function [str] = export_row(A, dims)
% recursively print matrix

% init output string
str = '';

% have we finished yet?
if length(dims) > 1
    % if not, then go to next layer
    for i=1:dims(1)
        % this test is just to make sure that we do not reshape a
        % one-dimensional array
        if length(dims) > 2
            % print next slice inside curly-braces
            str = sprintf('%s{%s},\n', str, export_row(reshape(A(i,:), dims(2:end)), dims(2:end)) );
        elseif length(dims) == 2
            % we are almost at the end, so do not use reshape, but stil
            % print in curly braces
            str = sprintf('%s{%s},\n', str, export_row(A(i,:), dims(2:end)) );
        end
    end
else
    % we have found one of the final layers, so print numbers
    str = sprintf('%.15f, ', A);
    % strip last space and comma
    str = str(1:end-2);
end
% strip final comma and return
str = sprintf('%s', str(1:end-1));
end
