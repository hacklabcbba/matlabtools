function [x, y, cflag, eflag, icnt] = regulafalsi_mod(f, c, xa, xb, tol, imax)
ya = feval(f, xa, c);
yb = feval(f, xb, c);
cflag = 0;
eflag = 0;
icnt = 0;

if abs(ya) < tol
    x = xa; y = ya; cflag = 1;
elseif abs(yb) < tol
    x = xb; y = yb; cflag = 1;
elseif ya*yb < 0
    for i=1:imax
        x = (xa*yb - xb*ya)/(yb - ya);
        y = feval(f, x, c);
        
        cnd1 = (abs(y) < tol);
        cnd2 = (abs(xb - xa) < tol);
        cnd3 = (abs(xb - xa) < (1/2)*abs(xb+xa)*tol);
        cflag = cnd1 || cnd2 || cnd3;
        if cflag
            break;
        elseif y*ya < 0
            rperf = abs((x-xb)/(x-xa));
            if rperf < 1
                ya = (ya*yb)/(yb+y);
            end
            xb = x; yb = y;
        else
            rperf = abs((x-xa)/(x-xb));
            if rperf < 1
                yb = (ya*yb)/(ya+y);
            end
            xa = x; ya = y;
        end
    end
    icnt = i;
else
    x = xa; y = ya; eflag = 1;
end
end

function [y] = feval(f, x, c)
y = f(x) - c;
end